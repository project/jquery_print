INTRODUCTION
------------
The jQuery Print module provides a library to access the jQuery Print plugin.
This module doesn't do very much by itself, but serves as a way for other
modules to easily use the jQuery Print plugin without having to define their
own libraries.


REQUIREMENTS
------------

This module requires the following modules:

 * Libraries API (https://www.drupal.org/project/libraries)
 * jQuery Update, running at LEAST jQuery 1.7 (https://www.drupal.org/project/jquery_update)


INSTALLATION
------------

 * Download the jQuery Print library from the following URL:
   https://github.com/DoersGuild/jQuery.print

 * Put the jQuery Print library into your libraries folder. The path file should
   be at the path sites/all/libraries/jquery-print/jquery.print.js

 * Install this module as you would normally install a contributed Drupal
   module. See: https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.


USAGE
------------

To use this module, simply load the library.

Example 1, using libraries_load:
libraries_load('jquery-print');

Example 2, using the form API:
$form['#attached']['libraries_load'][] = array('jquery-print');
